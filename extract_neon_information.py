#!/usr/bin/env python

"""
Extract the invest information of the NEON PDF sheet. 
"""

__author__ = "David Herzig"
__copyright__ = "Copyright 2023, The Day Trading Project"
__credits__ = ["David Herzig"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "David Herzig"
__email__ = "dave.herzig@gmail.com"
__status__ = "Experimental"

import pandas as pd
import pdfquery
import xml.etree.ElementTree as ET
import urllib.request
import os.path
import json

import data_retriever

def download_file(url, file):
    urllib.request.urlretrieve(url, file)


def extract_data(file):
    result = {}

    xml_file_name = 'neon_information.xml'

    pdf = pdfquery.PDFQuery(file)
    pdf.load()
    pdf.tree.write(xml_file_name, pretty_print = True)
    
    tree = ET.parse(xml_file_name)
    root = tree.getroot()
    
    company_names = []
    
    start_companies = False
    start_symbols = False
    for entry in root.iter('LTTextLineHorizontal'):
        if entry.text != None:
            value = entry.text.strip()
            if  value== 'Name':
                start_companies = True
                start_symbols = False
            elif value == 'ISIN':
                start_companies = False
                start_symbols = True
            elif start_companies:
                company_names.append(value)
            elif start_symbols:
                if len(company_names) > 0:
                    result[company_names[0]] = value
                    company_names.pop(0)
        else:
            for sub in entry.iter('LTTextBoxHorizontal'):
                value = sub.text.strip()
                if start_companies:
                    company_names.append(value)
                elif start_symbols:
                    if len(company_names) > 0:
                        if len(value) == 12:
                            result[company_names[0]] = value
                        company_names.pop(0)
              
    # TODO: Delete temporary file
    
    return result

def add_stock_basic_information(stock_data):
    cols = ['Company Name', 'ISIN', 'Symbol', 'Exchange', 'Sector', 'Industry', 'YF Score']
    rows = []
    for key, value in stock_data.items():
        basic_data = data_retriever.get_basic_data(value)
        exchange = basic_data['exchange']
        symbol = basic_data['symbol']
        yf_score = basic_data['score']
        sector = '' if 'sector' not in basic_data else basic_data['sector']
        industry = '' if 'industry' not in basic_data else basic_data['industry']
        new_row = [key, value, symbol, exchange, sector, industry, yf_score]
        rows.append(new_row)
    df = pd.DataFrame(rows, columns=cols)

    return df

def read_cached_data(file):
    if os.path.exists(file):
        with open(file) as f:
            data = json.load(f)
        df = pd.DataFrame.from_dict(data, orient='index')
        df = df.transpose()
        return df
    return None

def get_neon_information_basic():
    cache_file = r'neon_invest_information_basic.json'
    data = read_cached_data(cache_file)
    if data is not None:
        print('basic data from cache')
        return data

    url = 'https://www.neon-free.ch/media/neon_invest_stocks_etfs.pdf'
    filename = 'neon_invest_stocks_etfs.pdf'
    download_file(url, filename)
    stock_data = extract_data(filename)
    df = add_stock_basic_information(stock_data)
    df.to_json(r'neon_invest_information_basic.json')
    return df

def get_neon_information_advanced(basic_information):
    cache_file = r'neon_invest_information_advanced.json'
    data = read_cached_data(cache_file)
    if data is not None:
        print('advanced data from cache')
        return data

    all_rows = []
    for index, row in basic_information.iterrows():
        symbol = row['Symbol']
        data = data_retriever.get_advanced_data(symbol)
        json_data_dict = data.info
        all_rows.append(json_data_dict)

    final_df = pd.DataFrame(all_rows)
    final_df.to_json(r'neon_invest_information_advanced.json')
    return final_df

if __name__ == "__main__":
    df = get_neon_information_basic()