#!/usr/bin/env python

"""
Main function for data analysis
"""

__author__ = "David Herzig"
__copyright__ = "Copyright 2023, The Day Trading Project"
__credits__ = ["David Herzig"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "David Herzig"
__email__ = "dave.herzig@gmail.com"
__status__ = "Experimental"

import pandas as pd
import matplotlib.pyplot as plt
import json

import data_retriever
import extract_neon_information

if __name__ == '__main__':

    basic_information = extract_neon_information.get_neon_information_basic()

    print(basic_information)

    advanced_information = extract_neon_information.get_neon_information_advanced(basic_information)

    print(advanced_information)

