#!/usr/bin/env python

"""
Data visualization
"""

__author__ = "David Herzig"
__copyright__ = "Copyright 2023, The Day Trading Project"
__credits__ = ["David Herzig"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "David Herzig"
__email__ = "dave.herzig@gmail.com"
__status__ = "Experimental"

import pandas as pd
import matplotlib.pyplot as plt

import extract_neon_information

if __name__ == '__main__':

    basic_information = extract_neon_information.get_neon_information_basic()
    advanced_information = extract_neon_information.get_neon_information_advanced(basic_information)

    basic_information.Sector.value_counts().plot(kind='barh')
    plt.title('Number of appearances in dataset')
    plt.xlabel('Frequency')
    plt.show()

    advanced_information.industryKey.value_counts().plot(kind='pie')
    plt.axis('equal')
    plt.title('Number of appearances in dataset')
    plt.show()
