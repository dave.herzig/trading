#!/usr/bin/env python

"""
Retrieves financial data from Yahoo
"""

__author__ = "David Herzig"
__copyright__ = "Copyright 2023, The Day Trading Project"
__credits__ = ["David Herzig"]
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "David Herzig"
__email__ = "dave.herzig@gmail.com"
__status__ = "Experimental"

import requests
import yfinance as yf
import pandas as pd
import json

def get_basic_data(isin):
    url = "https://query2.finance.yahoo.com/v1/finance/search"
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    params = {'q': isin, 'quotesCount': 1, 'newsCount': 0}

    r = requests.get(url, params=params, headers=headers)
    data = r.json()
    return data['quotes'][0]

def get_advanced_data(stock_symbol):
    data = yf.Ticker(stock_symbol)
    return data

def get_historical_data(stock_symbol, start_date, end_date):
    stock_data = yf.download(stock_symbol, start=start_date, end=end_date)
    print(type(stock_data))
    print(stock_data)
    return stock_data
     

if __name__ == "__main__":
    data = get_advanced_data('ABBN.SW')
    json_data_dict = data.info
    print(json_data_dict)
    df = pd.DataFrame.from_dict(json_data_dict, orient='index')
    
    print(df)
